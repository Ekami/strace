/*
** strace.h for strace in /home/godard_b/workspace/Projets/En_cours/strace/rendu/src
** 
** Made by tuatini godard
** Login   <godard_b@epitech.net>
** 
** Started on  Tue Apr 30 00:14:22 2013 tuatini godard
** Last update Fri May 17 00:09:04 2013 tuatini godard
*/

#ifndef		STRACE_H_
# define	STRACE_H_
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <signal.h>
#include <sys/ptrace.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/reg.h>
#include <sys/user.h>
#include <sys/syscall.h>
#include <unistd.h>
typedef struct user_regs_struct	t_registers;

typedef union	u_instr_dec
{
  long		value;
  unsigned char	byte[sizeof(long)];
}		t_instr_dec;

int	start_strace_pid(pid_t pid);
int	start_strace_prog(char *progname, char *argv[], char *env[]);
bool	find_syscall(pid_t pid, t_registers *regs);
bool	print_syscall(unsigned long sys_number, t_registers *regs);

#endif /* !STRACE_H_ */
