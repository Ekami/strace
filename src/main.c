/*
** main.c for strace in /home/godard_b/workspace/Projets/En_cours/strace/rendu/src
** 
** Made by tuatini godard
** Login   <godard_b@epitech.net>
** 
** Started on  Tue Apr 30 00:13:57 2013 tuatini godard
** Last update Mon May  6 02:42:57 2013 tuatini godard
*/

#include "libelf.h"
#include "strace.h"

int	get_process_id(int ac, char **av)
{
  if (ac == 3)
    {
      if (strcmp(av[1], "-p") == 0)
	return (atoi(av[2]));
    }
  return (-1);
}

int	main(int ac, char **av, char **env)
{
  int	pid;
  char	*args;

  pid = get_process_id(ac, av);
  if (pid == -1)
    {
      if (ac >= 2 && strcmp(av[1], "-p") != 0)
	{
	  start_strace_prog(av[1], &(av[1]), env);
	  return (1);
	}
      printf("Usage: ./strace [-p pid] | progname\n");
      return (-1);
    }
  start_strace_pid(pid);
  return (0);
}
