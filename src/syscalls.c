/*
** syscalls.c for strace in /home/godard_b/workspace/Projets/En_cours/strace/rendu/src
** 
** Made by tuatini godard
** Login   <godard_b@epitech.net>
** 
** Started on  Sat May  4 11:59:19 2013 tuatini godard
** Last update Fri May 17 02:35:57 2013 tuatini godard
*/

#include "strace.h"

extern bool	g_ko_flag;

static bool	is_syscall(t_instr_dec *inst)
{
  unsigned char	x64_syscall_opcode[] = {0x0F, 0x05};
  unsigned char	x86_syscall_opcode[] = {0xcd, 0x80};
  unsigned char	sysenter_opcode[] = {0x0f, 0x34};

  if ((inst->byte[0] == x64_syscall_opcode[0] &&
      inst->byte[1] == x64_syscall_opcode[1]) ||
    (inst->byte[0] == x86_syscall_opcode[0] &&
     inst->byte[1] == x86_syscall_opcode[1]) ||
      (inst->byte[0] == sysenter_opcode[0] &&
     inst->byte[1] == sysenter_opcode[1]))
    return (true);
  return (false);
}

static void	detach_process(pid_t pid)
{
  printf("Process %d detached\n <detached ...>\n", pid);
  ptrace(PTRACE_DETACH, pid, NULL, NULL);
  exit(0);
}

bool		find_syscall(pid_t pid, t_registers *regs)
{
  t_instr_dec	inst;
  unsigned long	syscallFound;
  int		status;

  syscallFound = -1;
  while (43)
    {
      if (g_ko_flag)
	detach_process(pid);
      memset(&inst, 0, sizeof(t_instr_dec));
      ptrace(PTRACE_SINGLESTEP, pid, NULL, NULL);
      waitpid(pid, &status, 0);
      if (WIFEXITED(status))
	return (false);
      if (ptrace(PTRACE_GETREGS, pid, NULL, regs) == -1)
	return (false);
      inst.value = ptrace(PTRACE_PEEKDATA, pid, regs->rip, NULL);
      if (syscallFound != -1)
	{
	  print_syscall(syscallFound, regs);
	  syscallFound = -1;
	}
      if (is_syscall(&inst))
	syscallFound = (unsigned long) regs->rax;
    }
}
