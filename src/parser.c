/*
** args_parser.c for strace in /home/godard_b/workspace/Projets/En_cours/strace/rendu/src
** 
** Made by tuatini godard
** Login   <godard_b@epitech.net>
** 
** Started on  Sun May  5 15:50:20 2013 tuatini godard
** Last update Mon May 13 04:46:03 2013 tuatini godard
*/

#include "strace.h"

static void	print_selected_ret(char *ret, t_registers *regs)
{
  if (strcmp("int", ret) == 0 || strcmp("size_t", ret) == 0 ||
      strcmp("ssize_t", ret) == 0 || strcmp("pid_t", ret) == 0 ||
      strcmp("gid_t", ret) == 0 || strcmp("long", ret) == 0)
    printf(" = %ld", regs->rax);
  else if (strcmp("void*", ret) == 0)
    printf(" = %p", regs->rax);
  else if (strcmp("char*", ret) == 0)
    printf(" = %s", regs->rax);
  else
    printf(" = 0");
}

static void	print_proto_return_value(char *prototype, t_registers *regs)
{
  int		i;
  char		ret[15];

  i = 0;
  memset(ret, 0, 15);
  while (i < 15)
    {
      if (prototype[i] == ' ')
	i = 15;
      else
	ret[i] = prototype[i];
      i++;
    }
  prototype[strlen(prototype) - 1] = 0;
  printf("%s", prototype + strlen(ret) + 1);
  print_selected_ret(ret, regs);
  printf("\n");
}

bool		print_syscall(unsigned long sys_number, t_registers *regs)
{
  int		i;
  char		*line;
  size_t	len;
  ssize_t	read;
  FILE		*sys_fp;

  i = 0;
  len = 0;
  read = 0;
  sys_fp = fopen("ressources/syscalls", "r");
  if (sys_fp == NULL)
    return (false);
  while (read != -1)
    {
      read = getline(&line, &len, sys_fp);
      if (read != -1)
	{
	  if (i == sys_number)
	    print_proto_return_value(line, regs);
	}
      i++;
    }
}
