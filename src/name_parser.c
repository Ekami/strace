/*
** parser.c for strace in /home/godard_b/workspace/Projets/En_cours/strace/rendu/src
** 
** Made by tuatini godard
** Login   <godard_b@epitech.net>
** 
** Started on  Sun May  5 02:26:26 2013 tuatini godard
** Last update Sun May  5 23:09:59 2013 tuatini godard
*/

#include "strace.h"

static bool	parted_print_syscall_name(char *line,
					  int i, unsigned long sys_number)
{
  unsigned long	tmp_num;
  int		j;

  j = i;
  while (line[j])
    {
      if (line[j] == ' ')
	{
	  line[j] = 0;
	  tmp_num = atoi(&(line[j + 1]));
	  if (tmp_num == sys_number)
	    {
	      printf("%s", &(line[i]));
	      return (true);
	    }
	}
      j++;
    }
  return (false);
}

bool		print_syscall_name(unsigned long sys_number)
{
  FILE		*file;
  char		line[80];
  int		i;

  file = fopen("/usr/include/asm/unistd_64.h", "r");
  if (file == NULL)
    return (false);
  while (fgets(line, 80, file) != NULL)
    {
      i = 0;
      while (line[i])
	{
	  if (strncmp(&(line[i]), "__NR_", 5) == 0)
	    {
	      i += 5;
	      if (parted_print_syscall_name(line, i, sys_number))
		return (true);
	    }
	  i++;
	}
    }
  return (false);
}
