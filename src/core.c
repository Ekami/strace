/*
** core.c for strace in /home/godard_b/workspace/Projets/En_cours/strace/rendu/src
** 
** Made by tuatini godard
** Login   <godard_b@epitech.net>
** 
** Started on  Thu May  2 19:13:24 2013 tuatini godard
** Last update Fri May 17 02:39:28 2013 tuatini godard
*/

#include "strace.h"

bool		g_ko_flag;

static void	sig_handler(int signal)
{
  if (signal == SIGINT)
    g_ko_flag = true;
}

int		start_strace_pid(pid_t pid)
{
  t_registers	regs;

  g_ko_flag = false;
  signal(SIGINT, sig_handler);
  if (ptrace(PTRACE_ATTACH, pid) == -1)
    {
      fprintf(stderr, "Could not trace such process\n");
      return (-1);
    }
  printf("Process %d attached\n", pid);
  printf("restart_syscall(<... resuming interrupted call ...>) = 0\n");
  return (find_syscall(pid, &regs));
}

int		start_strace_prog(char *progname, char *argv[], char *env[])
{
  pid_t		pid;
  t_registers	regs;

  pid = fork();
  if (pid == -1)
    {
      fprintf(stderr, "Cannot fork such process\n");
      return (-1);
    }
  else if (pid == 0)
    {
      ptrace(PTRACE_TRACEME);
      kill(getpid(), SIGSTOP);
      execvpe(progname, argv, env);
    }
  else
    find_syscall(pid, &regs);
}
