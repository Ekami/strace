##
## Makefile for Makefile in /home/godard_b//workspace/epitech/
## 
## Made by tuatini godard
## Login   <godard_b@epitech.net>
## 
## Started on  Mon Oct 22 12:05:00 2012 tuatini godard
## Last update Mon May 13 03:55:51 2013 tuatini godard
##

SRC=		src/main.c \
		src/core.c \
		src/syscalls.c \
		src/parser.c

OBJS=		$(SRC:.c=.o)

NAME=		strace

CC=		cc

INC=		-Iincludes/

CFLAGS=		$(INC)

LIBS=	-Llibs/ -lelf

all:	$(NAME)

debug:	fclean
	clang $(SRC) $(LIBS) -g3 -lefence -o $(NAME)

$(NAME): $(OBJS)
	$(CC) $(OBJS) $(LIBS) -o $(NAME)

re:     fclean all

fclean: clean
	rm -f $(NAME)

clean:
	rm -f $(OBJS)
